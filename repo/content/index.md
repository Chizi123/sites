+++
title = "Joel's Package Repository"
+++

Welcome to my Arch Linux Ppackage Repository.
[Here](/x86_64) is a link to the repo for x86_64.
Currently I only provide packages for x86_64 Arch.
The packages are built every two hours with [this script I wrote](https://github.com/Chizi123/arch-autobuild-repo).

The repo is signed with the key "54C7B13F1CA2A9E0", from the file [pub_key.asc](/pub_key.asc).
You can add it as follows:
```shell
curl -O https://repo.joelg.net/pub_key.asc
pacman-key --add pub_key.asc
pacman-key --lsign-key 54C7B13F1CA2A9E0
```

Additionally you need to add the following to your `/etc/pacman.conf`:
```shell
[Chizi123]
Server=https://repo.joelg.net/x86_64
```

Should there be any problems, feel free to send me an email at [joelgrun@gmail.com](mailto:joelgrun@gmail.com).
