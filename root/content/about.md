+++
#title = "Joel's site"
+++

# About me
Hi, welcome to my site.
I am an Electrical Engineering Masters Student at the University of Melbourne, with a Batchelor of Science (Electrical Systems) and a Diploma in Computing. 

My interests lie in Computer Hardware Design and the operations of tech companies.

I avidly read tech sector publications, to monitor product developments and to keep up with the actions of the people developing those products.
Additionally, I also try to follow Australian and global politics and finance.

My [formal resume](/resume.pdf) is available here.
Please feel free to reach out to me at [joelgrun@gmail.com](mailto:joelgrun@gmail.com).

# My profiles on various sites
* [GitHub](https://github.com/Chizi123)
* [LinkedIn](https://www.linkedin.com/in/joel-grunbaum)
* [Keybase](https://keybase.io/joeleg)

# My sites
I run my own:
* [Blog](https://blog.joelg.net) - Containing occasional posts
* [Arch Linux Package Repository](https://repo.joelg.net) - My repository described [below](#arch-linux-package-autobuilderhttpsgithubcomchizi123arch-autobuild-repo)
* [Git server](https://git.joelg.net) - Staging server and hosting more personal projects
* [Wiki](https://wiki.joelg.net) - Personal Wiki with notes and tricks

# Some of my projects
## Personal
### [Arch Linux Package Autobuilder](https://github.com/Chizi123/Arch-autobuild-repo)
A script I wrote to automatically build packages from the AUR (Arch User Repository), and to assemble them into a repository.
The script has a simple configuration file which enables package signing, should that be required.
The feature I'm particularly proud of is the ability to build packages in parallel, allowing the script to run much quicker at the cost of system resources.
I use it to build my own repository every two hours.

### [dnscomp - a DNS benchmark tool](https://github.com/Chizi123/dnscomp)
A simple DNS benchmarking program written entirely in C from scratch. 
The program tests against a known list of websites and has a number of popular DNS servers, but allows more to be added on invocation. 
Servers are tested in parallel and feedback is proviede during execution, before the final results are presented.

### [Verilog CPU on FPGA](https://github.com/Chizi123/ELEN30010-Project)
A project for a university subjects to implement a CPU on an FPGA.
This was a great experience to learn the fundamentals of hardware design and assembly programming while being guided along the way.

### [Simplified C to MIPS compiler](https://github.com/Chizi123/Compuational-complexity-C-compiler)
A project I completed on exchange at the University of Edinburgh to write a C to MIPS assembly compiler written in Java.
The assignment was guided but the vast majority of implimentation details were self-implimented.

### [Various personal websites](#my-sites)
I have created and maintain of a number of websites for my own use.
I host on my own server and utilise various cloud providers.
Through this, I have experimented with different web servers, virtualisation, docker, DNS configuration and Linux administration.

## Open-source
### [Termux packages](https://github.com/termux/termux-packages)
Termux is an open source terminal environment for android with its own build and packaging system.
I have added and maintained some packages for the program, working within its unique build environment and tooling.

### [AUR packages](https://aur.archlinux.org/account/joeleg)
The AUR (Arch User Repository) is a community collection of build scripts for packages within the Arch Linux distribution.
I have added my own scrips and taken over maintenance of others.

### [LLVM clang-format patch](https://reviews.llvm.org/p/Chizi123/)
LLVM is an open source compiler toolchain, and clang is the C/C++ front end.
I submitted a patch for clang-format, a formatting tool for C/C++, enabling a more complex invocation option.
